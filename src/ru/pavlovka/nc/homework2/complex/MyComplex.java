package ru.pavlovka.nc.homework2.complex;

import java.util.Objects;

public class MyComplex {

    private double real = 0.0;
    private double imag = 0.0;

    public MyComplex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public MyComplex(){}

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public double getImag() {
        return imag;
    }

    public void setImag(double imag) {
        this.imag = imag;
    }

    public void setValue(double real, double imag){
        this.real = real;
        this.imag = imag;
    }

    @Override
    public String toString() {
        if(imag>=0) {
            return "(" + real + "+" + imag + "i)";
        }
        return "(" + real + imag + "i)";
    }

    public boolean isReal(){
        return real!=0;
    }

    public boolean isImaginary(){
        return imag!=0;
    }

    public boolean equals(double real, double imag){
        return (this.real==real && this.imag==imag);
    }

    public boolean equals(MyComplex another) {
        return this.equals(another.real, another.imag);
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(real, 2)+Math.pow(imag, 2));
    }

    public Double argument(){
        if(real>0){
            return Math.atan(imag/real);
        }

        if(real<0){
            if(imag>=0) {
                return Math.PI+Math.atan(imag/real);
            }else{
                return -Math.PI+Math.atan(imag/real);
            }
        }

        if(real==0){
            if(imag>0){
                return Math.PI/2;
            }else if(imag<0){
                return -Math.PI/2;
            }
        }

        return null;
    }

    public MyComplex add(MyComplex right){
        real+=right.getReal();
        imag+=right.getImag();
        return this;
    }

    public MyComplex addNew(MyComplex right){
        return new MyComplex(this.getReal(), this.getImag()).add(right);
    }

    public MyComplex subtract(MyComplex right){
        real-=right.getReal();
        real-=right.getImag();
        return this;
    }

    public MyComplex subtractNew(MyComplex right){
        return new MyComplex(this.getReal(), this.getImag()).subtract(right);
    }

    public MyComplex multiply(MyComplex right){
        real=real*right.getReal()-imag*right.getImag();
        imag=real*right.getReal()+imag*right.getImag();
        return this;
    }

    public MyComplex divide(MyComplex right){
        real=(real*right.getReal()+imag*right.getImag())/(Math.pow(right.getReal(), 2)+Math.pow(right.getImag(), 2));
        imag=(right.getReal()*imag-real*right.getImag())/(Math.pow(right.getReal(), 2)+Math.pow(right.getImag(), 2));
        return this;
    }

    public MyComplex conjugate(){
        return new MyComplex(real, -imag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyComplex myComplex = (MyComplex) o;
        return Double.compare(myComplex.real, real) == 0 &&
                Double.compare(myComplex.imag, imag) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(real, imag);
    }
}
