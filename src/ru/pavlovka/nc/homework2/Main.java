package ru.pavlovka.nc.homework2;

import ru.pavlovka.nc.homework2.complex.MyComplex;
import ru.pavlovka.nc.homework2.physics.Ball;
import ru.pavlovka.nc.homework2.physics.Container;
import ru.pavlovka.nc.homework2.polynomials.MyPolinomial;

public class Main {

    public static void main(String[] args) {
        tstComplex();
        tstPolinomial();
        tstBall();
    }

    public static void tstComplex(){
        MyComplex myComplex = new MyComplex();
        printComplex(myComplex);
        printComplex(myComplex.addNew(new MyComplex(1.0,3.0)));
        printComplex(myComplex);
        printComplex(myComplex.add(new MyComplex(1.0,3.0)));
        printComplex(myComplex.conjugate());
        System.out.println(myComplex.conjugate().equals(1.0, -3.0));
    }

    public static void tstPolinomial(){
        System.out.println(new MyPolinomial(1, 3, 6).add(new MyPolinomial(3,5)));
        System.out.println(new MyPolinomial(1, 3, 6).multiple(new MyPolinomial(3,5)));
    }

    public static void tstBall(){
        Ball ball=new Ball(2, 2, 2, 1, 90);
        Container container=new Container(0,0, 10, 10);
        System.out.println(container);
        int collissions=0;
        while(collissions<4){
            System.out.println(ball);
            if(!container.collides(ball)){
                System.out.println("Collision!");
                collissions++;
                ball.reflectVertical();
                ball.reflectHorizontal();
            }
            ball.move();
            System.out.println("Move to: "+ball);
        }
    }

    public static void printComplex(MyComplex complex){
        System.out.println(complex);
        System.out.println("isReal "+complex.isReal());
        System.out.println("IsImaginary "+complex.isImaginary());
        System.out.println("magnitude "+complex.magnitude());
        System.out.println("argument "+complex.argument());
    }
}
