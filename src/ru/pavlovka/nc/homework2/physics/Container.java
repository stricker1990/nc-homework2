package ru.pavlovka.nc.homework2.physics;

import java.util.Objects;

public class Container {

    private int x1;
    private int x2;
    private int y1;
    private int y2;

    public Container(int x, int y, int width, int height) {
        this.x1 = x;
        this.x2 = x+width;
        this.y1 = y;
        this.y2 = y+height;
    }

    public int getX() {
        return x1;
    }

    public int getWidth() {
        return x2-x1;
    }

    public int getY() {
        return y1;
    }

    public int getHeight() {
        return y2-y1;
    }

    @Override
    public String toString() {
        return "Container[("+x1+","+y1+"),("+x2+","+y2+")]";
    }

    public boolean collides(Ball ball){
        return collides(ball.getX(), ball.getY())
                && collides(ball.getX()+ball.getRadius(), ball.getY())
                && collides(ball.getX()-ball.getRadius(), ball.getY())
                && collides(ball.getX(), ball.getY()+ball.getRadius())
                && collides(ball.getX(), ball.getY()-ball.getRadius());
    }

    public boolean collides(float x, float y){
        return x<x2 && x>x1 && y<y2 && y>y1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Container container = (Container) o;
        return x1 == container.x1 &&
                x2 == container.x2 &&
                y1 == container.y1 &&
                y2 == container.y2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x1, x2, y1, y2);
    }
}
