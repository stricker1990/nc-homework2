package ru.pavlovka.nc.homework2.physics;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Ball {

    private static final int MAX_DIRECTION=180;

    private float x;
    private float y;
    private int radius;
    private float xDelta;
    private float yDelta;

    public Ball(float x, float y, int radius, int speed, int direction) {
        this.x = x;
        this.y = y;
        this.radius = radius;

        xDelta=calculateDeltaX(speed, direction);
        yDelta=calculateDeltaY(speed, direction);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public float getXDelta() {
        return xDelta;
    }

    public void setXDelta(float xDelta) {
        this.xDelta = xDelta;
    }

    public float getYDelta() {
        return yDelta;
    }

    public void setYDelta(float yDelta) {
        this.yDelta = yDelta;
    }

    public void move(){
        x+=xDelta;
        y+=yDelta;
    }

    public void reflectHorizontal(){
        xDelta=-xDelta;
    }

    public void reflectVertical(){
        yDelta=-yDelta;
    }

    @Override
    public String toString() {
        return "Ball[("+x+","+y+"),speed=("+xDelta+","+yDelta+")]";
    }

    private float calculateDeltaX(int speed, int direction){
        return (float) (speed*Math.cos(Math.toRadians(maxDirection(direction))));
    }

    private float calculateDeltaY(int speed, int direction){
        return (float) (-speed*Math.sin(Math.toRadians(maxDirection(direction))));
    }

    private int maxDirection(int direction){
        if(Math.abs(direction)>MAX_DIRECTION){
            if(direction<0) {
                return -MAX_DIRECTION;
            }else{
                return MAX_DIRECTION;
            }
        }
        return direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return Float.compare(ball.x, x) == 0 &&
                Float.compare(ball.y, y) == 0 &&
                radius == ball.radius &&
                Float.compare(ball.xDelta, xDelta) == 0 &&
                Float.compare(ball.yDelta, yDelta) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, radius, xDelta, yDelta);
    }
}
