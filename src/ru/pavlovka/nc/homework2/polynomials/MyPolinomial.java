package ru.pavlovka.nc.homework2.polynomials;

import java.util.Arrays;

public class MyPolinomial {

    private double[] coeffs;

    public MyPolinomial(double ...coeffs) {
        this.coeffs = coeffs.clone();
    }

    public int getDegree(){
        return coeffs.length-1;
    }

    public double getCoeff(int degree){
        if(degree>=coeffs.length){
            return 0;
        }
        return coeffs[degree];
    }

    @Override
    public String toString() {
        StringBuilder builder=new StringBuilder();
        for(int i=coeffs.length-1; i>=0; i--){
            builder.append(coeffs[i]).append((i == 0) ? "" : "x^" + i + "+");
        }
        return builder.toString();
    }

    public double evaluate(double x){
        double sum = 0;
        for(int i=0; i<coeffs.length; i++){
            sum+=coeffs[i]*Math.pow(x, i);
        }
        return sum;
    }

    public MyPolinomial add(MyPolinomial right){

        int maxDegree = Math.max(getDegree(), right.getDegree());
        double[] result=new double[maxDegree+1];

        for(int degree=0; degree<=maxDegree; degree++){
            result[degree] = getCoeff(degree) + right.getCoeff(degree);
        }

        return new MyPolinomial(result);
    }

    public MyPolinomial multiple(MyPolinomial right){

        int maxDegree = getDegree()+right.getDegree();
        double[] result = new double[maxDegree+1];

        for(int i=0; i<=getDegree(); i++){
            for(int j=0; j<=right.getDegree(); j++){
                int degree = i+j;
                result[degree]+=getCoeff(i)*right.getCoeff(j);
            }
        }
        return new MyPolinomial(result);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyPolinomial that = (MyPolinomial) o;
        return Arrays.equals(coeffs, that.coeffs);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coeffs);
    }
}
